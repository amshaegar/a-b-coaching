#!/bin/bash

function convert_haml {
  file="$1"
  file_base=`basename $file .haml`
  ./haml2html $file www/$file_base.html
  echo "[$(date)] $file"
}

function convert_all {
  for file in `ls templates/*.haml | grep -v layout.haml`
  do
    convert_haml "$file"
  done
}

function remove_html {
  for file in `ls templates/*.haml | grep -v layout.haml`
  do
    file_base=`basename $file .haml`
    rm www/$file_base.html
  done
}

function watch_templates {
  inotifywait -m -e close_write,moved_to,create templates |
  while read -r directory events file
  do
    if [[ "$file" =~ .haml$ ]]
    then
      if [[ "$file" == "layout.haml" ]]
      then
        convert_all
      else
        convert_haml "$directory$file"
      fi
    fi
  done
}

if [ -z "$1" ]
then
  convert_all
elif [ "$1" == "--clean" ]
then
  remove_html
elif [ "$1" == "--watch" ]
then
  watch_templates
else
  echo "Unknown argument: '$1'"
fi
